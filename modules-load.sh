#!/bin/bash
# modules-load.sh: copy the modules in /etc/modules-load.d/* to /etc/conf.d/modules and modprobe them
##
#  Copyright (C) 2014-2015 Aaditya Bagga (aaditya_gnulinux@zoho.com)
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed WITHOUT ANY WARRANTY;
#  without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
##

# /etc/modules-load.d/ is a directory containing files ending with .conf, which specify the modules to be loaded.

# However, openrc loads modules specified in the file /etc/conf.d/modules (by parsing the 'modules' variable).

# This script aims to copy the modules specified in individual files in /etc/modules-load.d/* to a single 'modules' variable
# in /etc/conf.d/modules and load (modprobe) them.

# Display some help text
if [ "$1" ]; then
	echo "modules_load: compatability modules loader for OpenRC"
	echo "Reads from /etc/modules-load.d/* to /etc/conf.d/modules"
	echo ""
	echo "Syntax: modules_load (takes no command line arguments)"
	echo "If no modules are loaded then gives no output."
	exit 0
fi

modules_loc="/etc/modules-load.d"
openrc_mod="/etc/conf.d/modules"

# Read modules by parsing $openrc_loc
if [ -e "$openrc_mod" ]; then
	source "$openrc_mod"
else
	echo "$openrc_mod not found"
	exit 1
fi

# Store the original modules for reference
modules_orig="$modules"

# Process the modules from /etc/modules-load.d/*
process_module () {
	# This function accepts a module as argument and checks if that module
	# is already present in the modules variable, else adds it.
	new_module="$1" # module to be checked specified as first argument
	if ! echo "$modules" | grep -q "$new_module"; then
		# Try to load this module
		modprobe "$new_module"
		if [ $? -eq 0 ]; then
			echo "Loaded $new_module"
		else
			echo "Unable to load $new_module"
			return 1
		fi
		# Check if previously $modules was empty
		if [ -z "$modules" ]; then
			modules="$new_module"
		else
			# Supplement the modules variable with this new module
			modules+=" $new_module"
		fi
	fi
}

write_modules () {
	# Check whether modules changed or not
	if [ ! "$modules" = "$modules_orig" ]; then
		# Write the modified modules back to $openrc_mod
		if grep -q 'modules=' $openrc_mod; then
			# The file contains the 'modules' variable
			sed -i "s/^.*modules=.*/modules=\"${modules}\"/g" "$openrc_mod" || return 1
		else
			echo "modules=\"$modules\"" >> $openrc_mod || return 1
		fi
	fi
}

# Main loop
for file in $modules_loc/*; do
	while read -r line; do
		# Check for blank lines and comments
		if [ -z "$line" ]; then
			continue  # skip blank line
		elif [ "$(echo "$line" | cut -c 1)" = "#" ]; then
			continue  # skip comment
		else
			process_module "$line"
		fi
	done < "$file"
done

# Write modules to $openrc_mod
write_modules

# Done
exit $?

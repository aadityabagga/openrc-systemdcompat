Some systemd compatibility components for OpenRC.

## Currently implemented:

systemctl: openrc wrapper for systemd's systemctl

modules_load: load modules from /etc/modules-load.d/* to /etc/conf.d/modules

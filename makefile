NAME = openrc-systemdcompat
DESTDIR =
INSTALL = /usr/bin/install
BINDIR = /usr/bin

install_modules-load: modules-load.sh
	$(INSTALL) -Dm 755 modules-load.sh $(DESTDIR)$(BINDIR)/modules_load
install_systemctl: systemctl.sh
	$(INSTALL) -Dm 755 systemctl.sh $(DESTDIR)$(BINDIR)/systemctl
